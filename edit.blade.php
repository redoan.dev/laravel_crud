@extends('blogs.layout')
@php
// @dd($blog->checkbox);
$values = explode(",", $blog->checkbox);
@endphp  
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Blog</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ url('blogs') }}"> Back</a>
            </div>
        </div>
    </div>
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div><br />
    @endif
    <form method="post" action="{{action('App\Http\Controllers\BlogController@update', $blog)}}">
      @csrf
      <input name="_method" type="hidden" value="PATCH">
      <div class="row">
        <div class="col-md-4"></div>
        <div class="form-group col-md-4">
          <label for="name">CoinName:</label>
          <input type="text" class="form-control" name="coinname" value="{{$blog->coinname}}">
        </div>
      </div>
      <div class="row">
        <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <label for="price">CoinPrice:</label>
            <input type="text" class="form-control" name="coinprice" value="{{$blog->coinprice}}">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4"></div>
          <div class="form-group col-md-4" style="margin-left:38px">
                <label for="toss">Toss</label>
               <lable>Keep</lable>
                 <input type="radio" name="radio" value="keep"  @if($blog->radio == 'keep') checked @endif>
               <lable>Port</lable>
                 <input type="radio" name="radio" value="port"  @if($blog->radio == 'port') checked @endif>
          </div>
      </div>
      <div class="row">
        <div class="col-md-4"></div>
          <div class="form-group col-md-4" style="margin-left:38px">
              <lable>Level</lable>
              <select name="dropdown">
                <option value="beginner"  @if($blog->dropdown=="beginner") selected @endif>Beginner</option>
                <option value="intermediate"  @if($blog->dropdown=="intermediate") selected @endif>Intermediate</option>
                <option value="advance" @if($blog->dropdown=="advance") selected @endif>Advance</option>  
              </select>
          </div>
      </div>
       <div class="row">
        <div class="col-md-4"></div>
          <div class="form-group col-md-4" style="margin-left:38px">
             <lable>Exchanges :</lable>
                <label><input type="checkbox" value="coindesk" name="option[]" @if(in_array("coindesk", $values)) checked @endif>Coindesk</label>
             </div>
                 <label><input type="checkbox" value="coinbase" name="option[]"  @if(in_array("coinbase", $values)) checked @endif>CoinBase</label>
            </div>
                <label><input type="checkbox" value="zebpay" name="option[]" @if(in_array("zebpay", $values)) checked @endif>Zebpay</label>
             </div>
          </div>
      </div>
      <div class="row">
        <div class="col-md-4"></div>
        <div class="form-group col-md-4">
          <button type="submit" class="btn btn-success" style="margin-left:38px">Update</button>
        </div>
      </div>
    </form>
@endsection
