@extends('welcome')
@section('layout')

<table class="table table-striped">
        <thead>
          <tr>
            <th scope="col">Id</th>
            <th scope="col">Name</th>
            <th scope="col">Class</th>
            <th scope="col">Gender</th>
            <th scope="col">Hobbies</th>
            <th scope="col">Email</th>
            <th scope="col">Image</th>
          </tr>
        </thead>
        <tbody>
        <tr>
        <th scope="row">{{ $student->id }}</th>
            <td>{{ $student->name }}</td>
            <td>{{ $student->class }}</td>
            <td>{{ $student->gender }}</td>
            <td>{{ $student->hobbies }}</td>
            <td>{{ $student->email }}</td>
            <td><img src="{{asset( $student->image) }}" width="500px"></td>
          </tr>
        </tbody>
      </table>
      @endsection
