@extends('welcome')
@section('layout')
<div>
    <a href="{{ url('/student') }}"><h3>Back</h3></a>
</div>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div><br />
    @endif
    <form method="POST" action="{{ url('student') }}" enctype="multipart/form-data">
        @csrf
        <div class="form-group row">
            <label for="name" class="col-sm-2 col-form-label">Name</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="name">
            </div>
          </div>
        <div class="form-group row">
            <label for="email" class="col-sm-2 col-form-label">Email</label>
            <div class="col-sm-10">
              <input type="email" class="form-control" name="email">
            </div>
          </div>
          <div class="row">
               <p>Gender</p> 
          <div class="col-sm-2">
            <div class="form-check">
                <input class="form-check-input" type="radio" name="gender" value="male">
                <label class="form-check-label">
                  Male
                </label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="radio" name="gender" value="female">
                <label class="form-check-label" >
                  Female
                </label>
              </div>
            </div>
        </div>
        <div class="form-group col-md-4">
            <label for="class">Class</label>
            <select name="class" class="form-control">
            <option selected>Choose...</option>
              <option>6</option>
              <option>7</option>
              <option>8</option>
              <option>9</option>
              <option>10</option>
            </select>
          </div>
          <div class="row">
            <p>Hobbies</p> 
            <div class="col-sm-2">
          <div class="form-check">
            <input class="form-check-input" type="checkbox" name="hobbies[]" value="MobileGaming">
            <label class="form-check-label">
              Mobile Gaming
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" name="hobbies[]" value="Travelling">
            <label class="form-check-label">
              Travelling
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" name="hobbies[]" value="Playing">
            <label class="form-check-label">
              Playing
            </label>
          </div>
          <div class="form-check">
              <input class="form-check-input" type="checkbox" name="hobbies[]" value="Reading">
            <label class="form-check-label">
              Reading
            </label>
          </div>
        </div>
    </div>
    <div class="form-group row">
      <label for="image" class="col-sm-2 col-form-label">Image</label>
      <div class="col-sm-10">
        <input type="file" name="image" class="form-control" placeholder="image">
      </div>
    </div>
    <button class="btn btn-primary" type="submit" style="color: aliceblue">Submit</button>
    </form>
@endsection