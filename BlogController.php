<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index()
    {
        $blogs = Blog::paginate(5);
        return view('blogs.index', compact('blogs'));
    }
    public function create()
    {
        return view('blogs.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'coinname' => 'required',
            'coinprice'=> 'required|numeric',
          ]); 
          
          $form= new Blog();
          $form->coinname=$request->get('coinname');
          $form->coinprice=$request->get('coinprice');
          $checkbox = implode(",", $request->get('option'));
          $form->dropdown=$request->get('dropdown');
          $form->radio=$request->get('radio');
          $form->checkbox = $checkbox; 
          $form->save();
          return redirect('blogs')->with('success', 'Coin has been added');
    }

    public function show(Blog $blog)
    {
        return view('blogs.show', compact('blog'));
    }

    public function edit($id)
    {
        $blog = Blog::find($id);
        return view('blogs.edit', ['blog' =>$blog]);
    }

    public function update(Request $request,  $id)
    {
        $request->validate([
            'coinname' => 'required',
            'coinprice'=> 'required|numeric',
          ]); 
          $form = Blog::find($id);
          $form->coinname=$request->get('coinname');
          $form->coinprice=$request->get('coinprice');
          $checkbox = implode(",", $request->get('option'));
          $form->dropdown=$request->get('dropdown');
          $form->radio=$request->get('radio');
          $form->checkbox = $checkbox; 
          $form->save();
          return redirect('blogs');
    }

    public function destroy($id)
    {
        $form = Blog::find($id);
        $form->delete();
        return redirect('blogs')->with('success','Coin has been  deleted');
    }
}
