@extends('blogs.layout')
@php
// @dd($blog->checkbox);
$values = explode(",", $blog->checkbox);
@endphp 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Show blog</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ url('blogs') }}"> Back</a>
            </div>
        </div>
    </div>
   
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>CoinName:</strong>
                {{ $blog->coinname }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>CoinPrice:</strong>
                {{ $blog->coinprice }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Toss:</strong>
                {{ $blog->radio }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Level:</strong>
                {{ $blog->dropdown }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Exchanges:</strong>
                {{ implode(', ', $values) }}
            </div>
        </div>
    </div>
@endsection