<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Http\Request;
use App\Http\Requests\StudentRequest;

class StudentController extends Controller
{
    public function index()
    {
        $student=Student::all();
        return view('student.index', compact('student'));
    }

    public function create()
    {
        return view('student.create');
    }

    public function store(StudentRequest $request)
    {
        $input = $request->all();
        // dd($input);
        if ($image = $request->file('image')) {
            $input['image'] =  $image->store('images');
        }
        
        $checkb= implode(',',$request->get('hobbies'));
        $input['hobbies'] = $checkb;
        Student::create($input);
        return redirect('/student')->with('success','New Student Added');
    }

    public function show(Student $student)
    {
        return view('student.show', compact('student'));
    }

    public function edit(Student $student)
    {
        return view('student.edit',compact('student'));
    }

 
    public function update(Request $request, Student $student)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $input = $request->all();
  
        if ($image = $request->file('image')) {
            $input['image'] =  $image->store('images');
        }
        $checkb= implode(',',$request->get('hobbies'));
        $input['hobbies'] = $checkb;
        $student->update($input);
       
        return redirect('/student')->with('success','Student Updeted');
    }

    public function destroy(Student $student)
    {
        $student->delete();
        return redirect('/student')->with('success','Student Deleted');
    }
}
